var del = require('del');
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass')(require('sass'));
var browserSync = require('browser-sync').create();
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('gulp-cssnano');


gulp.task('sass', function () {
    return gulp.src('./src/scss/flex-grid.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(cssnano())
        .pipe(sourcemaps.write('./sourcemaps'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('copy', function () {
    return gulp.src('./src/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('clean', function () {
    return del([
        './dist'
    ]);
})

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: './dist'
        },
        port: 8080,
        open: true
    });
    gulp.watch('./src/scss/**/*.scss', gulp.series('sass'));
    gulp.watch('./src/*.html', gulp.series('copy'));
    gulp.watch('./dist/index.html').on('change', browserSync.reload);
});

gulp.task('development', gulp.series('clean', 'sass', 'copy', 'serve'));
gulp.task('production', gulp.series('clean', 'sass', 'copy'));