# Flex Grid
Flex Grid is a [CSS Flexible Box Layout (Flexbox)](https://developer.mozilla.org/de/docs/Web/CSS/CSS_Flexible_Box_Layout) powered grid system written in [SASS](https://sass-lang.com/) using [Blocks, Elements and Modifiers (BEM)](http://getbem.com/introduction/) syntax.

## Documentation
The documentation may be found at the [Flex Grid Wiki](https://gitlab.com/MarkusBraun/flex-grid/-/wikis/00-Table-of-contents)

## Issues
Please report all issues in this GitLab repository at [Flex Grid/Issues](https://gitlab.com/MarkusBraun/flex-grid/-/issues).